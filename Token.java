package microbit;

public class Token
{ 
	public enum Symbol {
		SHOW, NUMBER, INTEGER_LIT, LEDS, STRING, PAUSE, ON, START, REPEAT, END, STRING_LIT,
		MS, TIMES, DO, TEMPERATURE, DEG_C, FOREVER, BUTTON, BUTTON_LIT, PRESSED, EVENT_LIT, PLAY, TONE,
		NOTE_LIT, FOR, BEAT, IF, BOOLEAN_LIT, COMPARATOR, THEN, ELSE, LIGHT, LEVEL, LEDS_LIT
	};
	private Symbol symbol;
	private String string_value;
	private int int_value;
	private boolean isLiteral;
	private boolean[][] leds;
	
	public Token(Symbol s, String v) {
		symbol = s;
		string_value = v;
		isLiteral = true;
	}
	
	public Token(Symbol s, int v) {
		symbol = s;
		int_value = v;
		isLiteral = true;
	}
	
	public Token(Symbol s) {
		symbol = s;
		isLiteral = false;
	}
	
	public Token(boolean[][] l) {
		leds = l;
		symbol = Token.Symbol.LEDS_LIT;
	}
	
	public Symbol getSymbol() {
		return symbol;
	}
	
	public String getStringValue() {
		return string_value;
	}
	
	public int getIntValue() {
		return int_value;
	}
	
	public boolean isLiteral() {
		return isLiteral;
	}
	
	public void setLeds(boolean[][] showLeds) {
		leds = showLeds; //??
	}
	
	public boolean[][] getLeds() {
		return leds;
	}
};