package microbit;

import java.util.List;

public class Parser
{ 
	public class ParseException extends Exception {
		public ParseException(String message) {
			super(message);
		}
	}
	public void parse(List<Token> tokens, Instruction parent, List<boolean[][]> showLeds) throws ParseException
	{
		while (tokens.size() > 0) {
			Instruction instruction = new Instruction();
			System.out.println(tokens.get(0).getSymbol());
			switch(tokens.get(0).getSymbol()) {
				case END:
					return;
				case SHOW:
					System.out.println(tokens.get(1).getSymbol());
					switch(tokens.get(1).getSymbol()) {
						case NUMBER:
							instruction.setCommand(Instruction.Command.SHOW_NUMBER);
							instruction.addToken(tokens.get(2));
							if (tokens.get(2).getSymbol() == Token.Symbol.LIGHT) tokens.remove(0);
							tokens.remove(0); tokens.remove(0); tokens.remove(0);
							break;
						case LEDS:
							instruction.setCommand(Instruction.Command.SHOW_LEDS);
							instruction.addToken(new Token(showLeds.get(0)));
							showLeds.remove(0);
							tokens.remove(0); tokens.remove(0);
							break;
						case STRING:
							instruction.setCommand(Instruction.Command.SHOW_STRING);
							instruction.addToken(tokens.get(2));
							tokens.remove(0); tokens.remove(0); tokens.remove(0);
							break;
						default:
							throw new ParseException("Wrong token after SHOW");
					}
					break;
				case PAUSE:
					// next token should be MS, then int
					instruction.setCommand(Instruction.Command.PAUSE);
					tokens.remove(0);
					if (tokens.get(0).getSymbol() == Token.Symbol.MS) tokens.remove(0);
					instruction.addToken(tokens.get(0));
					tokens.remove(0);
					break;
				case ON:
					switch(tokens.get(1).getSymbol()) {
						case START:
							instruction.setCommand(Instruction.Command.ON_START);
							tokens.remove(0); tokens.remove(0);
							parse(tokens, instruction, showLeds);
							tokens.remove(0); // END
							break;
						case BUTTON:
							instruction.setCommand(Instruction.Command.ON_BUTTON_PRESS);
							instruction.addToken(tokens.get(2));
							tokens.remove(0); tokens.remove(0); tokens.remove(0); tokens.remove(0);
							parse(tokens, instruction, showLeds);
							tokens.remove(0); // END
							break;
						case EVENT_LIT:
							instruction.setCommand(Instruction.Command.ON_EVENT);
							instruction.addToken(tokens.get(1));
							tokens.remove(0); tokens.remove(0); tokens.remove(0);
							parse(tokens, instruction, showLeds);
							tokens.remove(0); // END
							break;
						default:
							throw new ParseException("Wrong token after ON");
					}
					break;
				case REPEAT:
					// next token should be INT_LIT
					instruction.setCommand(Instruction.Command.REPEAT);
					instruction.addToken(tokens.get(1));
					tokens.remove(0); tokens.remove(0); 
					if (tokens.get(0).getSymbol() == Token.Symbol.TIMES) tokens.remove(0);
					if (tokens.get(0).getSymbol() == Token.Symbol.DO) tokens.remove(0);
					parse(tokens, instruction, showLeds);
					tokens.remove(0); // END
					break;
				case FOREVER:
					instruction.setCommand(Instruction.Command.FOREVER);
					tokens.remove(0);
					parse(tokens, instruction, showLeds);
					tokens.remove(0); // END
					break;
				case PLAY:
					instruction.setCommand(Instruction.Command.PLAY_TONE);
					instruction.addToken(tokens.get(2));
					instruction.addToken(tokens.get(4));
					tokens.remove(0); tokens.remove(0); tokens.remove(0); tokens.remove(0); tokens.remove(0); tokens.remove(0);
					break;
				case IF:
					instruction.setCommand(Instruction.Command.IF_ELSE);
					instruction.addToken(tokens.get(1));
					tokens.remove(0); tokens.remove(0); tokens.remove(0);
					parse(tokens, instruction, showLeds);
					tokens.remove(0); // ELSE
					parse(tokens, instruction, showLeds);
					tokens.remove(0); // END
					break;
				default:
					// Ignore invalid token
					tokens.remove(0);
					continue;
					//throw new ParseException("Wrong token: " + tokens.get(0).getSymbol() + ", tokens size: " + tokens.size());
			}
			parent.addChild(instruction);
		}
	}
};