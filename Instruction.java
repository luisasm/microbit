package microbit;

import java.util.List;
import java.util.ArrayList;

public class Instruction
{ 
	public enum Command {
		SHOW_NUMBER, SHOW_LEDS, SHOW_STRING, PAUSE, ON_START, REPEAT,
		FOREVER, ON_BUTTON_PRESS, ON_EVENT, PLAY_TONE, IF_ELSE, ROOT
	};

	private Token value1;
	private Token value2;
	private Command command;
	private List<Instruction> children;
	
	public Instruction() {
		command = Command.ROOT; // any default value
		value1 = null;
		value2 = null;
		children = new ArrayList<>();
	}
	
	public void setCommand(Command c) {
		command = c;
	}
	
	public void addToken(Token token) {
		if (value1 == null) value1 = token;
		else value2 = token;
	}
	
	public void addChild(Instruction child) {
		children.add(child);
	}
	
	public void print(int level) {
		for (int i = 0; i < level; i++) {
			System.out.print("    ");
		}
		System.out.println(command.name());
		for (int i = 0; i < children.size(); i++) {
			children.get(i).print(level + 1);
		}
	}
	
	public Command getCommand() {
		return command;
	}
	
	public List<Instruction> getChildren() {
		return children;
	}
	
	public Token getToken1() {
		return value1;
	}
};