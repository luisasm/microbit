package microbit;

import java.util.List;
import java.util.ArrayList;

public class Tokenizer
{ 
	public List<Token> tokenize(String inputLines) {
		String[] stringTokens = inputLines.split("[^\\w\"]+");
		List<Token> tokens = new ArrayList<>();
		for (int i = 0; i < stringTokens.length; i++) {
			String val = stringTokens[i].toUpperCase();
			Token.Symbol symbol;
			Token token;
			try {
				symbol = Token.Symbol.valueOf(val);
				token = new Token(symbol);
			} catch (IllegalArgumentException ex) {  
				if (val.charAt(0) == '"') {
					token = new Token(Token.Symbol.STRING_LIT, stringTokens[i].substring(1, val.length() - 1));
					System.out.println(val.substring(1, val.length() - 1));
				} else if (val.matches("^-?\\d+$")) {
					int number = Integer.parseInt(val);
					token = new Token(Token.Symbol.INTEGER_LIT, number);
				} else if (val.equals("RE") && stringTokens[i+1].equals("peat")) {
					token = new Token(Token.Symbol.REPEAT);
					i++;
				} else {
					// Ignore invalid token.
					continue;
				}
		    }
			tokens.add(token);
		}
		return tokens;
	}
};