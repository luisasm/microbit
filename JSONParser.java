package microbit;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.*;
import org.json.*;
import org.apache.commons.io.*;

public class JSONParser
{ 
	private List<int[]> show;
	
	public JSONParser() {
		show = new ArrayList<int[]>();
	}
	
	public String parseJSON(String filename) {
		String ret = "";
		try {

			InputStream is = new FileInputStream(filename);
            String jsonTxt = IOUtils.toString(is);
            JSONObject json = new JSONObject(jsonTxt);

			JSONArray regions = json.getJSONArray("regions");
			for (int i = 0; i < regions.length(); i++) {
				JSONArray lines = regions.getJSONObject(i).getJSONArray("lines");

				for (int j = 0; j < lines.length(); j++) {
					JSONArray words = lines.getJSONObject(j).getJSONArray("words");

					for (int k = 0; k < words.length(); k++) {
						String text = words.getJSONObject(k).getString("text");
						System.out.println(text);
						if (text.equals("show") && k + 1 < words.length() && words.getJSONObject(k+1).getString("text").equals("leds")) {
							String boundingBox = words.getJSONObject(k).getString("boundingBox");
							List<String> boundingList = Arrays.asList(boundingBox.split(","));
							System.out.println(boundingList);
							int[] bounds = new int[4];
							for (int a = 0; a < 4; a++) {
								bounds[a] = Integer.parseInt(boundingList.get(a));
							}
							show.add(bounds);
						}
						ret += text + " ";
					}
				}
			}

		} catch (Exception e) {
			 e.printStackTrace();
		}
		return ret;
	}
	
	public List<int[]> getAllShowBoundingBoxes() {
		return show;
	}
};