package microbit;

public class TSGenerator
{ 
	private String padding(int level) {
		String ret = "";
		for (int i = 0; i < level; i++) {
			ret += "    ";
		}
		return ret;
	}
	
	public String generate(Instruction root, int level) {
		String ret = "";
		switch(root.getCommand()) {
			case ROOT: 
			case ON_START:
				for (int i = 0; i < root.getChildren().size(); i++) {
					ret += generate(root.getChildren().get(i), level + 1);
				}
				break;
			case SHOW_NUMBER:
				ret += padding(level) + "basic.showNumber(";
				if (root.getToken1().getSymbol() == Token.Symbol.LIGHT) {
					ret += "input.lightLevel()";
				} else if (root.getToken1().getSymbol() == Token.Symbol.TEMPERATURE) {
					ret += "input.temperature()";
				} else {
					ret += root.getToken1().getIntValue();
				}
				ret += ")\n";
				break;
			case REPEAT:
				ret += padding(level) + "for (let i = 0; i < " + root.getToken1().getIntValue() + "; i++) {\n";
				for (int i = 0; i < root.getChildren().size(); i++) {
					ret += generate(root.getChildren().get(i), level + 1);
				}
				ret += padding(level) + "}\n";
				break;
			case SHOW_STRING:
				ret += padding(level) + "basic.showString(\""+ root.getToken1().getStringValue() + "\")\n";
				break;
			case PAUSE:
				ret += padding(level) + "basic.pause("+ root.getToken1().getIntValue() + ")\n";
				break;
			case FOREVER:
				ret += padding(level) + "basic.forever(() => {\n";
				for (int i = 0; i < root.getChildren().size(); i++) {
					ret += generate(root.getChildren().get(i), level + 1);
				}
				ret += padding(level) + "})\n";
				break;
			case ON_BUTTON_PRESS:
				ret += padding(level) + "input.onButtonPressed(Button." + root.getToken1().getStringValue() +", () => {\n";
				for (int i = 0; i < root.getChildren().size(); i++) {
					ret += generate(root.getChildren().get(i), level + 1);
				}
				ret += padding(level) + "})\n";
				break;
			case ON_EVENT:
				ret += padding(level) + "input.onGesture(Gesture." + root.getToken1().getStringValue() + ", () => {\n";
				for (int i = 0; i < root.getChildren().size(); i++) {
					ret += generate(root.getChildren().get(i), level + 1);
				}
				ret += padding(level) + "})\n";
				break;
			case SHOW_LEDS:
				ret += padding(level) + "basic.showLeds(`\n";
				boolean[][] leds = root.getToken1().getLeds();
				for (int i = 0; i < 5; i++) {
					ret += padding(level + 1);
					for (int j = 0; j < 5; j++) {
						if (leds[j][i]) ret += ". ";
						else ret += "# ";
					}
					ret += "\n";
				}
				ret += padding(level) + "`)\n";
				break;
			case IF_ELSE: // doesn't make sense without conditionals
			case PLAY_TONE: // quite complicated to deal with enums
			default:
				throw new RuntimeException("Not implemented yet: " + root.getCommand().name());
		}
		return ret;
	}
};