package microbit;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

public class LEDRecognizer
{
	private static double squareWidth_By_ShowWidthRatio = 0.296;
    private static double showToSquaresGap_By_ShowWidthRatio = 0.148;
	private static double squareGap_By_ShowWidthRatio = 0.116;
	
	private boolean[][] getLED(BufferedImage img, int[] show) throws MalformedURLException, IOException {
		int showTopLeftX = show[0];
		int showTopLeftY = show[1];
		int showWidth = show[2];
		int showHeight = show[3];
		
		int squareWidth = (int) (squareWidth_By_ShowWidthRatio * showWidth); //248;
		int vertGap = showHeight + showTopLeftY + (int) (showToSquaresGap_By_ShowWidthRatio * showWidth); //552;
		int squareGap = (int) (squareGap_By_ShowWidthRatio * showWidth); //97;
		
		System.out.println(squareWidth);
		System.out.println(vertGap);
		System.out.println(squareGap);
				
		boolean[][] result = new boolean[5][5];
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 5; i++) {
				int midSquareX = showTopLeftX + (squareWidth / 2) + i * squareWidth + i * squareGap;
				int midSquareY = vertGap + (squareWidth / 2) + j * squareWidth + j * squareGap;
				int clr = img.getRGB(midSquareX, midSquareY); 
				int red = (clr & 0x00ff0000) >> 16;
				int green = (clr & 0x0000ff00) >> 8;
				int blue = clr & 0x000000ff;
				// white has high red and green and blue. black has low red and green and blue.
				if (red > 126 && blue > 126 && green > 126) {
					result[i][j] = true; // white
				} else {
					result[i][j] = false; // black or red or green or blue
				}
				System.out.print((result[i][j] ? "W" : "B") + " ");
			}
			System.out.println();
		}
		return result;
	}
	
	public List<boolean[][]> getLEDS(String inputImage, List<int[]> boundingBoxes) throws MalformedURLException, IOException {
		BufferedImage img = ImageIO.read(new URL(inputImage));
		List<boolean[][]> result = new ArrayList<boolean[][]>();
		
		for (int i = 0; i < boundingBoxes.size(); i++) {
			result.add(getLED(img, boundingBoxes.get(i)));
		}

		return result;
	}
};