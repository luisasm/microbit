package microbit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.ArrayList;
import java.io.*;
import java.net.*;
import java.util.*;


public class Skeleton
{ 
	public static void main(String [] args) throws IOException, MalformedURLException
	{	
		// Step 1. Accept input
		JSONParser jsonParser = new JSONParser();
		String input = jsonParser.parseJSON(args[0]);
		
		List<int[]> show = jsonParser.getAllShowBoundingBoxes();
		String imagePath = args[1]; //"http://i.imgur.com/0sLwD6S.jpg";
		
		LEDRecognizer ledRecognizer = new LEDRecognizer();
		List<boolean[][]> showLeds = ledRecognizer.getLEDS(imagePath, show);
		// Step 2. Feed input to Tokenizer
		Tokenizer tokenizer = new Tokenizer();
		List<Token> tokens = tokenizer.tokenize(input);
		// Step 3. Feed Tokenizer output to Parser
		Parser parser = new Parser();
		Instruction root = new Instruction();
		try {
			parser.parse(tokens, root, showLeds);
		} catch (Parser.ParseException exception) {
			System.out.println(exception);
		}
		root.print(0);
		// Step 4. Feed Parser output to TS generator
		TSGenerator tsGenerator = new TSGenerator();
		String output = tsGenerator.generate(root, -2);
		// Step 5. Output TS generator
		Files.write(Paths.get(args[2]), output.getBytes(), StandardOpenOption.CREATE);
	}
};