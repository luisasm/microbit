# My project's README

## Commands for JSON support

*To compile:*
javac -cp ".;C:\Users\t-qihuan\Downloads\json-20160810.jar;C:\Users\t-qihuan\Downloads\commons-io-2.5.jar;" microbit/*.java
javac -cp ".;C:\Users\t-lusanm\Downloads\json-20160810.jar;C:\Users\t-lusanm\Downloads\commons-io-2.5.jar;" microbit/*.java

*To run:*
java -cp ".;<json-library-jar>;<apache-commons-jar>;" microbit.Skeleton <JSON Input> <Image URL> <Output file>

JSON input is the path to a text file which contains the body of the JSON returned from calling the cognitive service API
The image URL is the URL of the image (used to detect which LEDS are selected for the show LEDS command)
The output file is where the generated Typescript output will be dumped

Example:
java -cp ".;C:\Users\t-qihuan\Downloads\json-20160810.jar;C:\Users\t-qihuan\Downloads\commons-io-2.5.jar;" microbit.Skeleton microbit/test_json_input.txt http://i.imgur.com/U1Hh0EA.jpg microbit/test_result.txt
java -cp ".;C:\Users\t-lusanm\Downloads\json-20160810.jar;C:\Users\t-lusanm\Downloads\commons-io-2.5.jar;" microbit.Skeleton microbit/test_json_input.txt http://i.imgur.com/U1Hh0EA.jpg microbit/test_result.txt

Download json library from: http://mvnrepository.com/artifact/org.json/json/20160810 
Download the standard apache commons IO jar

API keys: a81ad68b69394b289a4becf36a44f085   